package com.ncamc.constant;

/**
 * 通用常量信息
 *
 * @Author : hugaoqiang
 */
public class Constant
{

    public static final String SESSION_KEY = "session_key";

    public static final String PHONENUMBER = "phoneNumber";

    public static final String WX_USER_NOT_PHONE = "微信用户未绑定手机号";

    public static final String GET_ERROR = "获取失败";

}
