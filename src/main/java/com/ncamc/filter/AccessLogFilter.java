//package com.ncamc.filter;
//
//import com.ncamc.mapper.AccessLogMapper;
//import com.ncamc.pojo.AccessLog;
//
//import javax.annotation.Resource;
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.Timestamp;
//import java.time.LocalDateTime;
//
///**
// * @Author : hugaoqiang
// */
//@WebFilter(filterName = "AccessLogFilter", urlPatterns = "/*")
//public class AccessLogFilter implements Filter
//{
//
//    @Resource
//    private AccessLogMapper accessLogMapper;
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException
//    {
//        // 初始化方法
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
//    {
//        try
//        {
//            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
//
//            if (httpServletRequest.getRequestURI().equals("/auth/phone"))
//            {
//                AccessLog accessLog = new AccessLog();
//                accessLog.setAccessTime(Timestamp.valueOf(LocalDateTime.now()));
//                accessLog.setIpAddress(httpServletRequest.getRemoteAddr());
//                accessLog.setUrl(httpServletRequest.getRequestURL().toString());
//                accessLog.setHttpMethod(httpServletRequest.getMethod());
//                accessLog.setStatusCode(httpServletResponse.getStatus());
//
//                accessLogMapper.insert(accessLog);
//            }
//            chain.doFilter(request, response);
//        }
//        catch (Exception e)
//        {
//            throw new ServletException("Error logging access", e);
//        }
//    }
//
//    @Override
//    public void destroy()
//    {
//        // 销毁方法
//    }
//}
