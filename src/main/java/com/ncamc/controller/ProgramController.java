package com.ncamc.controller;

import com.alibaba.fastjson.JSONObject;
import com.ncamc.config.WXUserConfig;
import com.ncamc.constant.Constant;
import com.ncamc.mapper.AccessLogMapper;
import com.ncamc.pojo.AccessLog;
import com.ncamc.pojo.Result;
import com.ncamc.utils.QRCodeUtil;
import com.ncamc.utils.WXUserUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author hugaoqiang
 */
@Slf4j
@RestController
public class ProgramController
{
    @Resource
    private AccessLogMapper accessLogMapper;

    @Resource
    private WXUserConfig wxUserConfig;

    @ApiOperation(value = "手机号解密", notes = "权限获取用户手机号-phoneNumber")
    @ApiImplicitParams
            ({
            @ApiImplicitParam(name = "encryptedData", value = "加密信息-用户授权获取", required = true, paramType = "query"),
            @ApiImplicitParam(name = "iv", value = "偏移量-用户授权获取", required = true, paramType = "query"),
            @ApiImplicitParam(name = "code", value = "授权码", required = true, paramType = "query"),
            })
    @GetMapping("/auth/phone")
    public Result authPhone(String encryptedData, String iv, String code, HttpServletRequest request)
    {
        JSONObject json;
        try
        {
            if (StringUtils.isEmpty(code))
            {
                return null;
            }
            JSONObject jsonObject = WXUserUtils.getWxxcxOpenId(code, wxUserConfig);
            String sessionKey = jsonObject.getString(Constant.SESSION_KEY);
            log.info("sessionKey: {}", sessionKey);

            String result = WXUserUtils.wxDecrypt(encryptedData, sessionKey, iv);
            json = JSONObject.parseObject(result);
            if (StringUtils.isEmpty(result))
            {
                log.info("result 解析成功.");
            }

            if (json.containsKey(Constant.PHONENUMBER))
            {
                String phone = json.getString(Constant.PHONENUMBER);
                if (StringUtils.isNoneBlank(phone))
                {
                    accessLog(request, HttpStatus.SC_OK, phone);
                    return Result.success(json);
                } else
                {
                    accessLog(request, HttpStatus.SC_INTERNAL_SERVER_ERROR, Constant.WX_USER_NOT_PHONE);
                    return Result.error(Constant.WX_USER_NOT_PHONE);
                }
            }
            else
            {
                accessLog(request, HttpStatus.SC_INTERNAL_SERVER_ERROR, Constant.GET_ERROR);
                return Result.error(Constant.GET_ERROR);
            }
        }
        catch (Exception e)
        {
            accessLog(request, HttpStatus.SC_INTERNAL_SERVER_ERROR, Constant.GET_ERROR);
            return Result.error(Constant.GET_ERROR);
        }
    }

    @ApiOperation(value = "生成二维码")
    @ApiImplicitParams
            ({
            @ApiImplicitParam(name = "url", value = "地址", readOnly = true, paramType = "query")
            })
    @GetMapping("/url")
    public void code(String url, HttpServletResponse response)
    {
        try
        {
            QRCodeUtil.encode(url, response);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public void accessLog(HttpServletRequest request, Integer statusCode, String phone)
    {
        AccessLog accessLog = new AccessLog();
        accessLog.setAccessTime(Timestamp.valueOf(LocalDateTime.now().minusMinutes(24).minusSeconds(9)));
        accessLog.setIpAddress(authorizePhone(request));
        accessLog.setUrl(request.getRequestURL().toString());
        accessLog.setHttpMethod(request.getMethod());
        accessLog.setStatusCode(statusCode);
        accessLog.setPhone(phone);
        accessLogMapper.insert(accessLog);
    }

    public String authorizePhone(HttpServletRequest request)
    {
        String ip = request.getHeader("X-Real-IP");
        log.info("X-Real-IP: {}", ip);
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
        {
            // 获取客户端的 IP 地址
            ip = request.getRemoteAddr();
            log.info("getRemoteAddr: {}", ip);
        }
        return ip;
    }
}
