package com.ncamc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author hugaoqiang
 */
@Data
@Component
@ConfigurationProperties(prefix = "app")
public class WXUserConfig {

    private String appId;

    private String secret;

    private String grantType;

    private String url;

}