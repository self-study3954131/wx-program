package com.ncamc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hugaoqiang
 */
@SpringBootApplication
@MapperScan("com.ncamc.mapper")
//@ServletComponentScan 使WebFilter注解生效，不加此注解spring不会将WebFilter注解标识的类加入到spring中管理
public class Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class);
    }

}
