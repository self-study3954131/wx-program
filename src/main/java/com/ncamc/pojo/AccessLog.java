package com.ncamc.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @Author : hugaoqiang
 * @CreateTime : 2023-06-06  13:24
 */
@Data
public class AccessLog
{

    private Date accessTime;

    private String ipAddress;

    private String url;

    private String httpMethod;

    private Integer statusCode;

    private String phone;

}
