package com.ncamc.pojo;

import lombok.Data;

/**
 * @author hugaoqiang
 */
@Data
public class Result
{

    private int code;

    private String message;

    private Object data;

    public Result(String message)
    {
        this.message = message;
    }

    public Result(Object data)
    {
        this.data = data;
    }

    public static Result success(Object json)
    {
        return new Result(json);
    }

    public static Result error(String message)
    {
        return new Result(message);
    }
}
