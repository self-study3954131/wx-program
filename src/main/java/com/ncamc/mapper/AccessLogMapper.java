package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.pojo.AccessLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @author gientech
 */
public interface AccessLogMapper extends BaseMapper<AccessLog>
{
    @Update("update access_log set phone = #{phone} where ip_address = #{ipAddress}")
    void save(@Param("ipAddress") String ipAddress, @Param("phone") String phone);
}
